=== Japanese font for TinyMCE ===
Contributors: raspi0124
Tags: TinyMCE,fonts,font,Japanese,JapaneseFont
Requires at least: 4.7
Tested up to: 4.9
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add Japanese font to TinyMCE Advanced plugin's font family selections.

== Description ==
Add Japanese font to TinyMCE Advanced plugin's font family selections.<br>
Font to be added；<br>
<a href="http://hp.vector.co.jp/authors/VA039499/#hui">ふい字</a><br>
<a href="https://www.google.com/get/noto/#sans-jpan">Noto Sans Japanese</a><br>
<a href="http://tanukifont.com/esenapaj/">エセナパJ</a><br>
<a href="https://github.com/adobe-fonts/source-han-sans/blob/master/README-JP.md">源ノ角ゴシック</a><br>
And If you want more font,please post a comment to page below.<br>
<br>
Please see <a href="http://raspi-diary.com/wordpress%e3%81%ae%e3%82%a8%e3%83%87%e3%82%a3%e3%82%bf%e3%81%ab%e6%97%a5%e6%9c%ac%e8%aa%9e%e3%83%95%e3%82%a9%e3%83%b3%e3%83%88%e3%82%92%e8%bf%bd%e5%8a%a0%e3%81%99%e3%82%8b%e3%83%97%e3%83%a9%e3%82%b0/">here</a> for more infomation.(Japanese)<br>
Development for this plugin takes place at GitHub. To report bugs or feature requests, please use <a href="https://github.com/raspi0124/Japanese-font-for-TinyMCE">Github</a> issues.

== Installation ==
Same as other plugins.

== Frequently Asked Questions ==
Q ;The plugin does not work
A ;Please report it at github issue page or forum.

== Changelog ==
Version beta-1 ;First release
Version beta-2 ;Add light and bold font for Noto Sans Japanese.
Version beta-3 ;Add light weight font for ふい字.(1.5MB to 400KB.)
Version beta-4 ;fixed some bug (it did not fix bug. its just made new bug.....why?)
Version beta-5 ;Fixed bug
Version beta-6 ;Fixed very important bug! Please update to this version as soon as possible please!
Version beta-7 ;Fixed bug temporary(temporary repair)
Version beta-8 ;Added エセナパJ (EsenapaJ) font
Version beta-9 ;Reverted Change
Version beta-10 ;I finally made it! I added エセナパJ (EsenapaJ) font! Enjoy!
Version 0.1 ;Finally, I made it! I updated this plugin to stable verdsion! With multi site support and compatible with WordPress version 4.7,4.8 and 4.9! Enjoy!
Version 0.2;Fixed bug temporary(temporary repair again..)
Version 0.3;FIxed bug and added logo for this plugin!
Version 0.4;Fixed an important bug! 
Version 0.5;... fixing bug again.. this is temporally repair. it's goin to work, but we still need to fix them..
Version 0.6; \Bug fixed
Version 0.7; I think all bug is fixed now. in next time, I will add font!
Version 0.8;Fixed Bug... I didn't add font..
Version 0.9;Fixed Bug
Version 1.0; Added 源ノ角ゴシック font and copyright of each font. it's version 1.x now! holey!
Version 1.1:Forgot to put link and licence! sorry..I now fixed it..
Version 1.2:I hope I fix bug now!
Version 1.3:is the bug fixed now...? I hope so...
Version 1.4:Fixed bug now! I hope there is no more bug!
Version 1.5: edited 説明書
Version 1.6:Fixed 説明書
Version 1.7;.I hate you, bug!Don't come to me anymore! please.. go somewhere else
Version1.8 Edited 説明書
Version 1.9 Added font! almost ready for version2.0! but Im too sleeeeeeeeeeepy..letme sleep.... If there is any bug,I will fix them morning....Goodnight and enjoy!
Version 2.0;Bug check complete. and added woff2 font for Huifont. that's it. done. and I might stop making update for while...prety sadly. if there is any important bug, just post a comment to <a href="https://raspi-diary.com/wordpress%e3%81%ae%e3%82%a8%e3%83%87%e3%82%a3%e3%82%bf%e3%81%ab%e6%97%a5%e6%9c%ac%e8%aa%9e%e3%83%95%e3%82%a9%e3%83%b3%e3%83%88%e3%82%92%e8%bf%bd%e5%8a%a0%e3%81%99%e3%82%8b%e3%83%97%e3%83%a9%e3%82%b0/" target="_blank">here</a> and I will fix (if I can...) them as fast as possible... If there is no responce from me for 1-2week, that's mean I can't responce.....but enjoy and I hope see you again soon!(really)
Version 2.05;Fixed important bug!
Version 2.1; say bye to 源ノ角ゴシック.. 源ノ角ゴシック has gone now.. The sentence using 源ノ角ゴシック will be replace by Noto Sans Japanese.
Version 2.15: edit 説明書
Version 2.16 edit 説明書
Version 2.17: edit 説明書
Version 2.18: edit 説明書
Version 2.2: Added CDN option(Alpha) you can now make your website bit more faster.To use CDN option, Please click here
